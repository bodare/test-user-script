import ls from "lightstreamer-client-node";
// import { createObjectCsvWriter } from "csv-writer";
import jwt from "jsonwebtoken";
import { pushtoQueue } from "./awshelper.js";
import dotEnv from "dotenv";

dotEnv.config();

const userCounts = process.env.USERS_COUNT ?? 1;
const serverNumber = process.env.SERVER_NO ?? 0;
const lsURL = process.env.LS_URL ?? "https://lightstreamer.ntb.one";

const lsConnections = [];
const report = [];

export function connectLS(quizID, userMobile, userToken) {
  const lsClient = new ls.LightstreamerClient(lsURL, "IQO_NODE");

  lsClient.connectionOptions.setHttpExtraHeadersOnSessionCreationOnly(true);
  lsClient.connectionOptions.setHttpExtraHeaders({ "IQO-QUIZ-ID": quizID });
  lsClient.connectionDetails.setUser(userMobile);
  lsClient.connectionDetails.setPassword(userToken);

  let startTime;
  lsClient.addListener({
    onServerError: (errorCode, errorMessage) => {
      console.error("Error Code", errorCode);
      console.error("Error Message", errorMessage);
    },
    onStatusChange: (status) => {
      if (status === "CONNECTED:WS-STREAMING") {
        const endTime = Date.now();
        console.log("time taken to connect (in ms)", endTime - startTime);
      } else if (status === "CONNECTING") {
        startTime = Date.now();
      }
    },
    onListenStart: (lsDataClient) => {
      console.log("Started Listening");
    },
    onListenEnd: (lsDataClient) => {
      console.log("Finished Listening");
    },
  });
  lsClient.connect();
  lsConnections.push(lsClient);

  const check = new Promise((resolve, reject) => {
    const optionTimer = subscribeToLS(lsClient, {
      quizID,
      name: "optionTimer",
      items: ["timer", "timestamp"],
      onItemUpdate: (itemInfo) => {},
      mobile: userMobile,
    });

    const warmUpTimer = subscribeToLS(lsClient, {
      quizID,
      name: "warmUpTimer",
      items: ["timer", "timestamp"],
      onItemUpdate: (itemInfo) => {},
      mobile: userMobile,
    });

    const preQuizAd = subscribeToLS(lsClient, {
      quizID,
      name: "preQuizAd",
      items: ["isActive"],
      onItemUpdate: (itemInfo) => {},
      mobile: userMobile,
    });

    const activeSessions = subscribeToLS(lsClient, {
      quizID,
      name: "preQuizAd",
      items: ["timestamp", "activeSessions"],
      onItemUpdate: (itemInfo) => {},
      mobile: userMobile,
    });

    const gameEnd = subscribeToLS(lsClient, {
      quizID,
      name: "gameEnd",
      items: ["timestamp", "gameEnded"],
      onItemUpdate: (itemInfo) => {
        const gameEnded = itemInfo.getValue("gameEnded");
        if (gameEnded) {
          //#region Un-subscribe to all events
          optionTimer();
          warmUpTimer();
          preQuizAd();
          activeSessions();
          gameEnd();
          question();
          option();
          answer();
          questionSeparator();
          gameInfo();
          optionAnalytics();
          //#endregion
          resolve(userMobile);
          cleanUpServer(0);
        }
      },
      mobile: userMobile,
    });

    const question = subscribeToLS(lsClient, {
      quizID,
      name: "question",
      items: [
        "timestamp",
        "question",
        "questionId",
        "positivePoints",
        "optionOnScreenTime",
        "description",
        "media",
        "mediaCredit",
      ],
      onItemUpdate: (itemInfo) => {},
      mobile: userMobile,
    });

    const option = subscribeToLS(lsClient, {
      quizID,
      name: "option",
      items: ["timestamp", "questionId", "options"],
      onItemUpdate: (itemInfo) => {},
      mobile: userMobile,
    });

    const answer = subscribeToLS(lsClient, {
      quizID,
      name: "answer",
      items: ["timestamp", "questionId", "answer"],
      onItemUpdate: (itemInfo) => {},
      mobile: userMobile,
    });

    const questionSeparator = subscribeToLS(lsClient, {
      quizID,
      name: "questionSeparator",
      items: ["timestamp", "questionNumber"],
      onItemUpdate: (itemInfo) => {},
      mobile: userMobile,
    });

    const gameInfo = subscribeToLS(lsClient, {
      quizID,
      name: "gameInfo",
      items: [
        "id",
        "warmupTimerStarted",
        "expectedStartTimeStamp",
        "expectedEndTimestamp",
        "gameStarted",
        "currentGameEngineStage",
        "gameEnded",
        "totalQuestions",
        "gameMedia",
      ],
      onItemUpdate: (itemInfo) => {},
      mobile: userMobile,
    });

    const optionAnalytics = subscribeToLS(lsClient, {
      quizID,
      name: "optionAnalytics",
      items: ["timestamp", "questionId", "analytics"],
      onItemUpdate: (itemInfo) => {},
      mobile: userMobile,
    });
  })
    .then(() => {
      console.log("Game Ended");
    })
    .catch((error) => {
      console.error("Error in Script", error);
    });
  return check;
}

/**
 *
 * @param {ls.LightstreamerClient} lsClient Lightstreamer connected client
 * @param {{quizID: string|number,name: string, items: string[], onItemUpdate: (itemInfo: ls.ItemUpdate) => void, mobile: string|number}} param1 params
 */
export function subscribeToLS(
  lsClient,
  { quizID, name, items, onItemUpdate, mobile }
) {
  const subscribe = new ls.Subscription(
    "DISTINCT",
    [`${quizID}_${name}`],
    items
  );

  subscribe.addListener({
    onItemUpdate: (updateInfo) => {
      console.log("Updated Data for", name, updateInfo);
      const data = [];
      updateInfo.forEachField((d) => {
        const itemName = d;
        const value = updateInfo.getValue(itemName);
        data.push({ [itemName]: value });
      });
      const userData = {
        mobile,
        quizID,
        subName: name,
        data: JSON.stringify(data),
        timestamp: Date.now(),
      };
      report.push(userData);
      // csvWriter.writeRecords([userData]);

      onItemUpdate(updateInfo);
    },
  });

  subscribe.setDataAdapter("DEFAULT");
  lsClient.subscribe(subscribe);
  return () => {
    lsClient.unsubscribe(subscribe);
  };
}

const createUsers = (numberOfUser) => {
  const users = [];
  for (let i = 0; i < numberOfUser; i++) {
    const userMobile = "5" + serverNumber + `${i}`.padStart(8, "0");
    const token = jwt.sign(
      {
        id: "197",
        name: "Piyush Goyal",
        username: "Piush007",
        email: "mohitgoyalmg95@yahoo.com",
        avatar:
          "https://foobucketlambdanew.s3.us-east-2.amazonaws.com/gender_profile/DefaultFemale.png",
        phone: userMobile,
        school_id: "365298",
        zone: "Ahmedabad",
        iat: 1666000941,
        exp: 4258000941,
      },
      "thisismysecretkeywhichmesachinanddwivediknows"
    );

    users.push({
      userMobile,
      userToken: token,
      quizID: process.env.QUIZ_ID,
    });
  }

  return users;
};

Promise.all(
  createUsers(userCounts).map((user) => {
    return connectLS(user.quizID, user.userMobile, user.userToken);
  })
)
  .then((data) => {})
  .catch((error) => console.error("Something went wrong", error));

const cleanUpServer = async (eventType) => {
  await pushtoQueue({
    subject: report,
  });
  lsConnections.map((client) => {
    try {
      if (client.disconnect) {
        client.disconnect();
        console.log(
          "Disconnected for user",
          client?.connectionDetails?.getUser()
        );
      }
    } catch (error) {
      console.error("Error in Disconnecting", error);
    }
  });
  process.exit(eventType);
};

[
  `exit`,
  `SIGINT`,
  `SIGUSR1`,
  `SIGUSR2`,
  `uncaughtException`,
  `SIGTERM`,
].forEach((eventType) => {
  process.on(eventType, cleanUpServer);
});
